// cleaning up Autonomous

package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.I2cAddr;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.hardware.DcMotorController;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.matrices.OpenGLMatrix;
import org.firstinspires.ftc.robotcore.external.matrices.VectorF;
import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.navigation.Orientation;
import org.firstinspires.ftc.robotcore.external.navigation.RelicRecoveryVuMark;
import org.firstinspires.ftc.robotcore.external.navigation.VuMarkInstanceId;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackable;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackableDefaultListener;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackables;


@Autonomous

public class AAAutonomous_straight_red_2_1 extends LinearOpMode {
    boolean backwardEnabled = true;
    private DcMotor armMotor;
    private DcMotor motor1;
    private DcMotor motor2;
    private Servo leftClaw;
    private Servo rightClaw;
    private Servo rightArm;
    private Servo leftArm;
    private ColorSensor rightColorSensor;
    private ColorSensor leftColorSensor;
    
    OpenGLMatrix lastLocation = null;
    VuforiaLocalizer vuforia;
    
    // phases
    final int ARMUP = 1; // "final" needed for the switch statement
    final int DRIVEFORWARD = 2;
    final int BACKUP = 3;
    final int BUMPBLOCK = 4;
    final int TINYBACKUP = 5;
    final int IDLE = 6;
    final int BUMP_BALL = 7;
    final int RESTORE_FROM_BALL = 8;
    final int READ_BALL_COLOR = 9;
    final int LITTLE_ARM_UP = 10;
        
    int currentPhase = IDLE;
    
    // directions
    final int GO_LEFT = 1;
    final int GO_MIDDLE = 2;
    final int GO_RIGHT = 3;
    
    int targetColumn = GO_LEFT;
    
    
    @Override
    public void runOpMode() {
        
        double leftMotorPower = 0;
        double rightMotorPower = 0;
        double leftTrigger = 0;
        double rightTrigger = 0;
        
        double TRIGGER_MULTIPLIER = 0.5;
        
        double MOTOR_POWER = 0.3; // autonomous driving speed
        
        // **** driving distances ****
        double BALL_ROTATION = 110;
        double TARGET_ROTATION = 1700; // how far to go initially
        double PULL_BACK_ROTATION = 100; // How far to roll backwards
        double PUSH_BLOCK_ROTATION = PULL_BACK_ROTATION * 2.5;  // push the block into the box (forcefully)
        double TINY_BACKUP_ROTATION = 15;  // backing up at the end of Autonomous so not touching block
        
        double BUMP_BALL_MULTIPLIER = 0.1;
        
        armMotor = hardwareMap.get(DcMotor.class, "armMotor");
        motor1 = hardwareMap.get(DcMotor.class, "motor1");
        motor2 = hardwareMap.get(DcMotor.class, "motor2");
        leftClaw = hardwareMap.get(Servo.class, "leftClaw");
        rightClaw = hardwareMap.get(Servo.class, "rightClaw");
        rightArm = hardwareMap.get(Servo.class, "rightArm");
        leftArm = hardwareMap.get(Servo.class, "leftArm" );
        int cameraMonitorViewId = hardwareMap.appContext.getResources().getIdentifier("cameraMonitorViewId", "id", hardwareMap.appContext.getPackageName());
        VuforiaLocalizer.Parameters parameters = new VuforiaLocalizer.Parameters(cameraMonitorViewId);
        rightColorSensor = hardwareMap.get(ColorSensor.class,"rightColorSensor" );
        rightColorSensor.setI2cAddress(I2cAddr.create8bit(0x3a));
        rightColorSensor.enableLed(true);
        leftColorSensor = hardwareMap.get(ColorSensor.class,"leftColorSensor" );
        leftColorSensor.setI2cAddress(I2cAddr.create8bit(0x3c));
        leftColorSensor.enableLed(true);
 
        parameters.vuforiaLicenseKey = "ATWJ0X//////AAAAGU1aaHH/uknnhXGSiwW/Jtsuq/BHiynMiFGatuxalj1s+5mgkhBeA4tGslEfeEbwrqmg8WykWo1OY66VwnMJPLVG6+eCHUrD6IpYWRfmH3Hewv8l6ja/gjiBVTrCuoo3LSucwSrq01RdGkPN+ATTRDEkin7wrnkTkHdwYRRjU1lL4zZ/xubc3lf9TtivUgoZBwe0pJXvwN+2GE0E5aWpbzYd4ky70mANBFNnV0SbmrVg96Jb7MCc0fBI2i/3ZTLpBP1TIx/UpJkih9OIxNG9aLPPCSHs99uP5txxuvhbkimo4HowQffZb5LZELqV9wdZ/BmrTT6LQg9poiEXTnfTgwlL1R++Gld8goJM+aU9Hill";

        
        //parameters.cameraDirection = VuforiaLocalizer.CameraDirection.BACK;
        parameters.cameraDirection = VuforiaLocalizer.CameraDirection.FRONT;
        this.vuforia = ClassFactory.createVuforiaLocalizer(parameters);
        
        RelicRecoveryVuMark vuMark;
            
        int armMotorStartPos = 0;
        int position = 0;
        int armMotorPos = 0;
        int arm_max;
        int arm_min;
        int motor1pos = 0; // raw position, read from motor encoder (can start anywhere)
        int motor2pos = 0;
        int m1rotation = 0;  // how far the wheel has turned since the start
        int m2rotation = 0;
        int m1startpos = 0;
        int m2startpos = 0;
        int m1boxpos = 0;
        int m2boxpos = 0;
        
        
        int FORWARD = 1;
        int BACKWARD = 3;
        int ballBumpDir = FORWARD;
        
        armMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        armMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        
        motor1.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motor2.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motor1.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        motor2.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        
        m1startpos = motor1.getCurrentPosition();
        m2startpos = motor2.getCurrentPosition();
        
        armMotorPos = armMotor.getCurrentPosition();
        armMotorStartPos = armMotorPos;
        arm_max = armMotorPos + 1000;
        arm_min  = armMotorPos - 1000;

        telemetry.addData("Arm Motor Position",armMotorPos);
        telemetry.addData("Status", "Initialized");
        telemetry.update();
        // Wait for the game to start (driver presses PLAY)
    
        VuforiaTrackables relicTrackables = this.vuforia.loadTrackablesFromAsset("RelicVuMark");
        VuforiaTrackable relicTemplate = relicTrackables.get(0);
        relicTemplate.setName("relicVuMarkTemplate"); // can help in debugging; otherwise not necessary

        telemetry.addData(">", "Press Play to start");
        telemetry.update();
        
        waitForStart();

        relicTrackables.activate();
        
        rightArm.setPosition(0.5);
        leftClaw.setPosition(0.7);
        rightClaw.setPosition(0.3);
        
        long setTime = System.currentTimeMillis();
       

        while(System.currentTimeMillis() - setTime < 1000 ) {
            // this line reads the pictograph
            vuMark = RelicRecoveryVuMark.from(relicTemplate);
            
            if (vuMark != RelicRecoveryVuMark.UNKNOWN) {

                if (vuMark == RelicRecoveryVuMark.RIGHT) {
                    targetColumn = GO_RIGHT;
                }
                else if (vuMark == RelicRecoveryVuMark.LEFT) {
                    targetColumn = GO_LEFT;
                }
                else if (vuMark == RelicRecoveryVuMark.CENTER) {      //
                    targetColumn = GO_MIDDLE;
                }
 
                telemetry.addData("VuMark", "%s visible", vuMark);            }
            else {
                telemetry.addData("VuMark", "not visible");
                targetColumn = GO_MIDDLE;
            }   
             telemetry.update();
             rightArm.setPosition(0.2);
             leftArm.setPosition(0.0);
             
        } // ends the one-second while loop
        
        currentPhase = READ_BALL_COLOR; //Starts phases
        
        
        //targetColumn = GO_RIGHT;
        //ballBumpDir = FORWARD;
                   
                       
        // run until the end of the match (driver presses STOP)
        while (opModeIsActive()) {
            
            //  do these things every time through the loop
            armMotorPos = armMotor.getCurrentPosition();
            motor1pos = motor1.getCurrentPosition();
            motor2pos = motor2.getCurrentPosition();
            m1rotation = motor1pos - m1startpos;
            m2rotation = motor2pos - m2startpos;
                
            // phases
            
            switch (currentPhase) {
                // to do: make a color sensor phase
                
                case READ_BALL_COLOR:
                    
                    rightArm.setPosition(0.2);
                    float red = rightColorSensor.red();
                    float blue = rightColorSensor.blue();
                    sleep(700);//code to wait for sensor to read color
                    
                    if (red > blue) {
                        ballBumpDir = FORWARD;
                        
                        telemetry.addData("I see red", 1 );
                        //telemetry.update();
                        
                        currentPhase = BUMP_BALL;
                    }
                    
                    else if (backwardEnabled == true) {
                    
                        ballBumpDir = BACKWARD;
                        
                        telemetry.addData("I see blue", 1 );
                        //telemetry.update();
                        
                        currentPhase = BUMP_BALL;
                    }
                    
                    else {
                        rightArm.setPosition(1.0);
                        leftArm.setPosition(0.0);
                        
                        currentPhase = ARMUP;
                    }
                    
                    break;
                
                 case LITTLE_ARM_UP:
                    // moves the arm up
                    armMotor.setPower(0.2);
                    leftColorSensor.enableLed(false);
                    rightColorSensor.enableLed(false);
                    // if the arm is high enough, then you stop it and 
                    // move to the next phase (drive forward)
                    if (armMotorPos > armMotorStartPos + 150) {
                        armMotor.setPower(0.0);
                        currentPhase = BUMP_BALL;
                     }   
                    break;
                    
                case BUMP_BALL:
                    if (ballBumpDir == FORWARD) {
                        motor1.setPower(-MOTOR_POWER * BUMP_BALL_MULTIPLIER);
                        motor2.setPower(MOTOR_POWER * BUMP_BALL_MULTIPLIER);
                        
                        if (m2rotation > BALL_ROTATION) { 
                            motor1.setPower(0.0);
                            motor2.setPower(0.0);
                            m1rotation = 0;
                            m2rotation = 0;
                            rightArm.setPosition(1.0);
                            rightColorSensor.enableLed(false);
                            leftColorSensor.enableLed(false);
                            currentPhase = ARMUP;
                            
                             
                        }   
                    }
                    else if (ballBumpDir == BACKWARD) {
                        motor1.setPower(MOTOR_POWER * BUMP_BALL_MULTIPLIER);
                        motor2.setPower(-MOTOR_POWER * BUMP_BALL_MULTIPLIER);
                        
                        if (m2rotation < -BALL_ROTATION) { 
                            motor1.setPower(0.0);
                            motor2.setPower(0.0);
                            m1rotation = 0;
                            m2rotation = 0;
                            rightArm.setPosition(1.0);
                            
                            currentPhase = RESTORE_FROM_BALL; 
                        }
                    }
                    break;
                    
                case RESTORE_FROM_BALL:
                    if (ballBumpDir == FORWARD) {
                        motor1.setPower(MOTOR_POWER * BUMP_BALL_MULTIPLIER);
                        motor2.setPower(-MOTOR_POWER * BUMP_BALL_MULTIPLIER);
                        
                        if (m2rotation < -BALL_ROTATION) { 
                            motor1.setPower(0.0);
                            motor2.setPower(0.0);
                            m1rotation = 0;
                            m2rotation = 0;
                            rightArm.setPosition(1.0);
                            currentPhase = ARMUP;
                             
                        }   
                    }
                    else if (ballBumpDir == BACKWARD) {
                        motor1.setPower(-MOTOR_POWER * BUMP_BALL_MULTIPLIER);
                        motor2.setPower(MOTOR_POWER * BUMP_BALL_MULTIPLIER);
                        
                        if (m2rotation > BALL_ROTATION) { 
                            motor1.setPower(0.0);
                            motor2.setPower(0.0);
                            m1rotation = 0;
                            m2rotation = 0;
                            rightArm.setPosition(1.0);
                            
                            currentPhase = ARMUP; 
                        }
                    }
                    break;
                
                case ARMUP:
                    // moves the arm up
                    armMotor.setPower(0.2);
                    leftColorSensor.enableLed(false);
                    rightColorSensor.enableLed(false);
                    // if the arm is high enough, then you stop it and 
                    // move to the next phase (drive forward)
                    if (armMotorPos > armMotorStartPos + 500) {
                        armMotor.setPower(0.0);
                        currentPhase = DRIVEFORWARD;
                     }   
                    break;
                    
                case DRIVEFORWARD:
                    
                    if (targetColumn == GO_LEFT && ballBumpDir == FORWARD) {
                        motor1.setPower(-MOTOR_POWER * 0.83); // Negative numbers for m1 = forward
                        motor2.setPower(MOTOR_POWER);                       
                    }
                   else if (targetColumn == GO_MIDDLE && ballBumpDir == FORWARD) {
                        motor1.setPower(-MOTOR_POWER * 0.925); // Nagative numbers for m1 = forward
                        motor2.setPower(MOTOR_POWER);                       
                    }
                   else if (targetColumn == GO_RIGHT && ballBumpDir == FORWARD) {
                        motor1.setPower(-MOTOR_POWER * 1.375); // Nagative numbers for m1 = forward
                        motor2.setPower(MOTOR_POWER);                       
                    }
                    else if (targetColumn == GO_LEFT && ballBumpDir == BACKWARD) {
                        motor1.setPower(-MOTOR_POWER * 0.85); // Negative numbers for m1 = forward
                        motor2.setPower(MOTOR_POWER);                       
                    }
                   else if (targetColumn == GO_MIDDLE && ballBumpDir == BACKWARD) {
                        motor1.setPower(-MOTOR_POWER * 0.95); // Nagative numbers for m1 = forward
                        motor2.setPower(MOTOR_POWER);                       
                    }
                   else if (targetColumn == GO_RIGHT && ballBumpDir == BACKWARD) {
                        motor1.setPower(-MOTOR_POWER * 1.15); // Nagative numbers for m1 = forward
                        motor2.setPower(MOTOR_POWER);                       
                    }
                    
                    // stops the motors when they get to the box
                    if (m1rotation < -TARGET_ROTATION) {
                        motor1.setPower(0);
                        motor2.setPower(0);
                    }

                    
                    // wait until both motors are stopped
                    // then drop the block
                    // reset the rotation
                    // change the phase to BACKUP
                    if (m1rotation < -TARGET_ROTATION) {
                        rightClaw.setPosition(0.6);
                        leftClaw.setPosition(0.4);
                        m1startpos = motor1pos; // initialize box pos
                        m2startpos = motor2pos;
                        m1rotation = 0; //reset current rotation
                        m2rotation = 0;
                        currentPhase = BACKUP;
                    }
                    
                    break;
                    
                case BACKUP:
                    motor1.setPower(MOTOR_POWER); // reverse
                    motor2.setPower(-MOTOR_POWER);
                    
                    if (m1rotation > PULL_BACK_ROTATION) {
                        motor1.setPower(0.0);
                        motor2.setPower(0.0);
                        
                        m1startpos = motor1pos; // initialize box pos
                        m2startpos = motor2pos;
                        m1rotation = 0; //reset current rotation
                        m2rotation = 0;
                    setTime = System.currentTimeMillis();    
                        currentPhase = BUMPBLOCK;
                    }
                    
                    
                    
                    break;
                    
                case BUMPBLOCK:
                    
                    while(System.currentTimeMillis() - setTime < 500 ) {
                    motor1.setPower(0.0);
                    motor2.setPower(0.0);
                    
                    }
                    motor1.setPower(-MOTOR_POWER); // negative numbers for m1 = forward
                    motor2.setPower(MOTOR_POWER);
                    
                    if (m1rotation < -PUSH_BLOCK_ROTATION) {
                        motor1.setPower(0.0);
                        motor2.setPower(0.0);
                        
                        m1startpos = motor1pos; // initialize box pos
                        m2startpos = motor2pos;
                        m1rotation = 0; //reset current rotation
                        m2rotation = 0;
                        currentPhase = TINYBACKUP;
                    }
                    
                    break;
                    
                case TINYBACKUP:
                    motor1.setPower(MOTOR_POWER / 4);
                    motor2.setPower(-MOTOR_POWER / 4);
                
                    if (m1rotation > TINY_BACKUP_ROTATION) {
                        motor1.setPower(0.0);
                        motor2.setPower(0.0); 
                        rightClaw.setPosition(0.8);
                        leftClaw.setPosition(0.2);
                        currentPhase = IDLE;
                    }
                    
                    break;
                    
                case IDLE:
                    // in case we want to do anything when done moving
                    break;
                
            } // switch currentPhase
            
        
        } // while opmodeisactive
            
    }
}
