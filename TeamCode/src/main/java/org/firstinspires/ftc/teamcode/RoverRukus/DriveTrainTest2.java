
package org.firstinspires.ftc.teamcode.RoverRukus;


import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.I2cAddr;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotorController;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.util.ElapsedTime;

/**
 * This file contains an minimal example of a Linear "OpMode". An OpMode is a 'program' that runs in either
 * the autonomous or the teleop period of an FTC match. The names of OpModes appear on the menu
 * of the FTC Driver Station. When an selection is made from the menu, the corresponding OpMode
 * class is instantiated on the Robot Controller and executed.
 *
 * This particular OpMode just executes a basic Tank Drive Teleop for a PushBot
 * It includes all the skeletal structure that all linear OpModes contain.
 *
 * Remove a @Disabled the on the next line or two (if present) to add this opmode to the Driver Station OpMode list,
 * or add a @Disabled annotation to prevent this OpMode from being added to the Driver Station
 */
@TeleOp
//@Disabled

public class DriveTrainTest2 extends LinearOpMode {

    // Propertiesr
    /**
     * Declares our actuators
     */
    private DcMotor motor1; // Left drive motor
    private DcMotor motor2; // Right drive motor



    @Override
    public void runOpMode() {
        // Properties
        // Gets hardware info and names each actuator
        motor1 = hardwareMap.get(DcMotor.class, "motor1"); // Left drive motor
        motor2 = hardwareMap.get(DcMotor.class, "motor2"); // Right drive motor
        //Names all variables, sets values
        double leftMotorPower = 0; // Used for controling power on the left drive motor.
        double rightMotorPower = 0; // Used for controling power on the right drive motor
        double leftTrigger = 0; // Used to measure how far the left trigger is pressed
        double rightTrigger = 0; // Used to measure how far the right trigger is pressed
        double TRIGGER_MULTIPLIER = 0.2; // Used to scale down arm rotation
        double WHEEL_MOTOR_POWER = 0.8; // the power to the wheels
        int position = 0;
        int motor1pos = 0; // Used to measure the left drive motor's position
        int motor2pos = 0; // Used to measure the right drive motor's position
        int m1rotation = 0; // Used as telemetry imput variable for the "motorrotation1 =" string
        int m2rotation = 0; // Used as telemetry imput variable for the "motorrotation2 =" string
        int m1startpos = 0; // Used to help control the left drive motor during autonomous driving
        int m2startpos = 0; // used to help control the right drive motor during autonomous driving

        //Configures motors to use encoders and brake when no power is applied
        motor1.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motor2.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motor1.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        motor2.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        // finds current position of all motors
        m1startpos = motor1.getCurrentPosition();
        m2startpos = motor2.getCurrentPosition();

        // Relays status to driver station
        telemetry.addData("Status", "Initialized");
        telemetry.update();


        // Wait for the game to start (driver presses PLAY)
        waitForStart();

        // Run until the end of the match (driver presses STOP)
        while (opModeIsActive()) {


            // Asigns the numbers representing how far the drive motors have rotated to m1rotation and m2rotation
            motor1pos = motor1.getCurrentPosition();
            motor2pos = motor2.getCurrentPosition();
            m1rotation = motor1pos - m1startpos;
            m2rotation = motor2pos - m2startpos;


            // Get gamepad variables
            leftMotorPower = this.gamepad1.left_stick_y * WHEEL_MOTOR_POWER;
            rightMotorPower = -this.gamepad1.right_stick_y * WHEEL_MOTOR_POWER;


            // Driving commands
            motor1.setPower(leftMotorPower);
            motor2.setPower(rightMotorPower);

            // Motor commands
            telemetry.addData("motorrotation1 =", m1rotation);
            telemetry.addData("motorrotation2 =", m2rotation);
            telemetry.update();

            idle();
        } // while op mode active
    } // runOpMode()
}

