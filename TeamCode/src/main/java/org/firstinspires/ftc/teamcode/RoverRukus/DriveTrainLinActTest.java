package org.firstinspires.ftc.teamcode.RoverRukus;
// test 10/23

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;

/**
 * This file contains an minimal example of a Linear "OpMode". An OpMode is a 'program' that runs in either
 * the autonomous or the teleop period of an FTC match. The names of OpModes appear on the menu
 * of the FTC Driver Station. When an selection is made from the menu, the corresponding OpMode
 * class is instantiated on the Robot Controller and executed.
 *
 * This particular OpMode just executes a basic Tank Drive Teleop for a PushBot
 * It includes all the skeletal structure that all linear OpModes contain.
 *
 * Remove a @Disabled the on the next line or two (if present) to add this opmode to the Driver Station OpMode list,
 * or add a @Disabled annotation to prevent this OpMode from being added to the Driver Station
 */
@TeleOp
//@Disabled

public class DriveTrainLinActTest extends LinearOpMode {

    // Properties
    /**
     * Declares our actuators
     */
    private DcMotor leftDriveMotor; // Left drive motor
    private DcMotor rightDriveMotor; // Right drive motor
    private DcMotor armExtendMotor; //  arm extending motor
    private DcMotor armRaiseMotor; //  arm raising motor
    //private DcMotor robotLiftMotor; // lifting motor
    //private DcMotor sweeperMotor; //block intake motor
    private Servo hookServo; // linear actuator hook
      //   private CRServo CRServo1;

    @Override
    public void runOpMode() {
        // Properties
        // Gets hardware info and names each actuator
        leftDriveMotor = hardwareMap.get(DcMotor.class, "leftDriveMotor"); // Left drive motor.
        rightDriveMotor = hardwareMap.get(DcMotor.class, "rightDriveMotor"); // Right drive motor
        armExtendMotor = hardwareMap.get(DcMotor.class, "armExtendingMotor"); // arm extending motor
        armRaiseMotor = hardwareMap.get(DcMotor.class, "armRaiseMotor"); // arm raising motor
        //robotLiftMotor = hardwareMap.get(DcMotor.class, "robotLiftMotor"); // robot lifting motor
        //sweeperMotor = hardwareMap.get(DcMotor.class, "robotSweepMotor"); // block intake sweeper motor

        hookServo = hardwareMap.get(Servo.class, "hookServo"); // servo motor


        //    CRServo1 = hardwareMap.get(CRServo.class, "CRServo1");

        //Names all variables, sets values
        double leftWheelPower = 0; // Used for controling power on the left drive motor.
        double rightWheelPower = 0; // Used for controling power on the right drive motor
        double armExtendPower = 0; // Used to extend the grabbbing arm
        double liftRobotPower = 0; // Used for controlling power on the lifting motor
        double armRaisePower = 0; // used to raise and lower the arm
        double robotLiftPower = 0;
        double sweeperMotorPower = 0;
        double leftTrigger = 0; // Used to measure how far the left trigger is pressed
        double rightTrigger = 0; // Used to measure how far the right trigger is pressed
        double TRIGGER_MULTIPLIER = 0.2; // Used to scale down arm rotation
        double WHEEL_POWER_MLUT = 0.8; // the power to the wheels
        double ARM_RAISE_POWER_MLUT = 0.05; // the power to the wheels
        double ARM_EXTEND_POWER_MLUT = 0.05; // the power to the arm extender
        double LIFT_ROBOT_POWER_MLUT = 1.0; // the power to the wheels
        double maxArmMult = 0.15; // the max power applied to the arm raising motor
        double armMultiplierSlope = maxArmMult / 100; //slope is the change in power divided by the distance traveled
        double SWEEPER_POWER_MULT = 1.0;
        int position = 0;

        int leftDriveMotorPos = 0; // Used to measure the left drive motor's position
        int rightDriveMotorPos = 0; // Used to measure the right drive motor's position
        int armRaiseMotorPos = 0; // used to measure the position of the arm

        //Configures motors to use encoders and brake when no power is applied
        leftDriveMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        rightDriveMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        armExtendMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        armRaiseMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        //robotLiftMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);




        leftDriveMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        rightDriveMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        armExtendMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        armRaiseMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        //robotLiftMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        leftDriveMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        rightDriveMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        armExtendMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        armRaiseMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        //robotLiftMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);


        // finds current position of all motors
//        m2startpos = rightDriveMotor.getCurrentPosition();

        // Relays status to driver station
        telemetry.addData("Status", "Initialized");
        telemetry.update();


        // Wait for the game to start (driver presses PLAY)
        waitForStart();

        // Run until the end of the match (driver presses STOP)
        while (opModeIsActive()) {



            leftDriveMotorPos = leftDriveMotor.getCurrentPosition();
            //           rightDriveMotorPos = rightDriveMotor.getCurrentPosition();
            armRaiseMotorPos = armRaiseMotor.getCurrentPosition();


            // Get gamepad variables
            leftWheelPower = this.gamepad1.left_stick_y * WHEEL_POWER_MLUT;
            rightWheelPower = -this.gamepad1.right_stick_y * WHEEL_POWER_MLUT;
            armExtendPower = this.gamepad1.right_stick_y * ARM_EXTEND_POWER_MLUT;
            if (this.gamepad2.left_stick_y > 0) {
                    ARM_EXTEND_POWER_MLUT = armMultiplierSlope * armRaiseMotorPos + maxArmMult; // this line will make the arm slow down as it moves up, so that it won't go too fast as it falls down
                }
                else if (this.gamepad2.left_stick_y < 0) {
                    ARM_EXTEND_POWER_MLUT = -armMultiplierSlope * armRaiseMotorPos;
            }

            armRaisePower = this.gamepad2.left_stick_y * ARM_RAISE_POWER_MLUT;
            robotLiftPower = this.gamepad2.right_stick_y * LIFT_ROBOT_POWER_MLUT;
            //sweeperMotorPower = this.gamepad2.left_trigger * SWEEPER_POWER_MULT;


            if (this.gamepad2.a) {
                hookServo.setPosition(0.15);
            } else if (this.gamepad2.b) {
                hookServo.setPosition(0.65);
            } else if (this.gamepad2.x) {
                hookServo.setPosition(0.315);
            } else if (this.gamepad2.y) {
                hookServo.setPosition(0.48);
            }


            // Driving commands
            leftDriveMotor.setPower(leftWheelPower);
            rightDriveMotor.setPower(rightWheelPower);
            //robotLiftMotor.setPower(robotLiftPower);
            armRaiseMotor.setPower(armRaisePower);
            armExtendMotor.setPower(armExtendPower);
            //sweeperMotor.setPower(sweeperMotorPower);

            //           rightDriveMotor.setPower(rightWheelPower);
            //         armExtendingMotor.setPower(armExtendPower);
            //       armRaiseMotor.setPower(armRaisePower);

        /*    if (this.gamepad1.a) {
                CRServo1.setDirection(DcMotor.Direction.FORWARD);
                CRServo1.setPower(1);
            } else if (this.gamepad1.b) {
                CRServo1.setDirection(DcMotor.Direction.REVERSE);
                CRServo1.setPower(1);
            }
            else {
                CRServo1.setPower(0);
            }

*/
            // Motor commands

            telemetry.update();

            idle();
        } // while op mode active
    } // runOpMode()
}
