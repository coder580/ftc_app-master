package org.firstinspires.ftc.teamcode.RoverRukus;
// test 10/23

// Basic Robot
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;
// Mineral Detection
import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.tfod.Recognition;
import org.firstinspires.ftc.robotcore.external.tfod.TFObjectDetector;
//REV IMU
import com.qualcomm.hardware.bosch.BNO055IMU;
import com.qualcomm.hardware.bosch.JustLoggingAccelerationIntegrator;
import org.firstinspires.ftc.robotcore.external.Func;
import org.firstinspires.ftc.robotcore.external.navigation.Acceleration;
import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.navigation.Orientation;
import org.firstinspires.ftc.robotcore.external.navigation.Position;
import org.firstinspires.ftc.robotcore.external.navigation.Velocity;

import java.util.Locale;
import java.util.List;


/**
 * This file contains an minimal example of a Linear "OpMode". An OpMode is a 'program' that runs in either
 * the autonomous or the teleop period of an FTC match. The names of OpModes appear on the menu
 * of the FTC Driver Station. When an selection is made from the menu, the corresponding OpMode
 * class is instantiated on the Robot Controller and executed.
 *
 * This particular OpMode just executes a basic Tank Drive Teleop for a PushBot
 * It includes all the skeletal structure that all linear OpModes contain.
 *
 * Remove a @Disabled the on the next line or two (if present) to add this opmode to the Driver Station OpMode list,
 * or add a @Disabled annotation to prevent this OpMode from being added to the Driver Station
 */
@Autonomous
//@Disabled

public class AutonomousV_0_3 extends LinearOpMode {

    // Properties

    // Mineral Sense
    private static final String TFOD_MODEL_ASSET = "RoverRuckus.tflite";
    private static final String LABEL_GOLD_MINERAL = "Gold Mineral";
    private static final String LABEL_SILVER_MINERAL = "Silver Mineral";
    private static final String VUFORIA_KEY = "AVya6t7/////AAABmTZybxAXJk+alcs417OOIRRBuvbJJJhz2do6X0fVrc7EBp+uaTnt3CtVJ1gYeSCRga45fI5Z+MOAOPQZJGH9xHZ4vl/0pi7baRH8VLygHowUZSmBIt9dWAhLPw2dXQx7ZcLgnyZo2gVfu1T5kJpfGVf6D/3ueuClxI+NsnqQLH8WAmFX7hvVElkbxFslx1XHv2xoQYys9URLfidSFh5No9yG8U2M+T2DU5h3QNF/tVBdFS/Mw/HVSh5gsxHQBba2WzIA0w5SH5T4y+ypYQfIJsgX7sCX8xrW/6uZc9qBREguqX4oCy8AL3YZugY1FIx4PcGL+F+7OBATPDug3PO2HEr81YJcLuk1bM+i2zKP4bAs";
    private VuforiaLocalizer vuforia;
    private TFObjectDetector tfod;
    // REV IMU
    // The IMU sensor object
    BNO055IMU imu;
    // State used for updating telemetry
    Orientation angles;

    //Actuators
    private DcMotor leftDriveMotor; // Left drive motor
    private DcMotor rightDriveMotor; // Right drive motor
    //    private DcMotor armExtendingMotor; //  arm extending motor
    //    private DcMotor armRaisingMotor; //  arm raising motor
    private DcMotor robotLiftMotor; // lifting motor
    private Servo hookServo; // hook servo
    // private CRServo CRServo1;

    @Override
    public void runOpMode() {
        // Properties
        // Set up the parameters with which we will use our IMU. Note that integration
        // algorithm here just reports accelerations to the logcat log; it doesn't actually
        // provide positional information.
        BNO055IMU.Parameters parameters = new BNO055IMU.Parameters();
        parameters.angleUnit           = BNO055IMU.AngleUnit.DEGREES;
        parameters.accelUnit           = BNO055IMU.AccelUnit.METERS_PERSEC_PERSEC;
        parameters.calibrationDataFile = "BNO055IMUCalibration.json"; // see the calibration sample opmode
        parameters.loggingEnabled      = true;
        parameters.loggingTag          = "IMU";
        parameters.accelerationIntegrationAlgorithm = new JustLoggingAccelerationIntegrator();

        // Retrieve and initialize the IMU. We expect the IMU to be attached to an I2C port
        // on a Core Device Interface Module, configured to be a sensor of type "AdaFruit IMU",
        // and named "imu".
        imu = hardwareMap.get(BNO055IMU.class, "imu");
        imu.initialize(parameters);
        // The TFObjectDetector uses the camera frames from the VuforiaLocalizer, so we create that
        // first.
        initVuforia();

        /*if (ClassFactory.getInstance().canCreateTFObjectDetector()) {
            initTfod();
        } else {
            telemetry.addData("Sorry!", "This device is not compatible with TFOD");
        } */

        // Gets hardware info and names each actuator
        leftDriveMotor = hardwareMap.get(DcMotor.class, "leftDriveMotor"); // Left drive motor
        rightDriveMotor = hardwareMap.get(DcMotor.class, "rightDriveMotor"); // Right drive motor
//        armExtendingMotor = hardwareMap.get(DcMotor.class, "armExtendingMotor"); // arm extending motor
//        armRaisingMotor = hardwareMap.get(DcMotor.class, "armRaisingMotor"); // arm raising motor
        robotLiftMotor = hardwareMap.get(DcMotor.class, "robotLiftMotor"); // robot lifting motor
        hookServo = hardwareMap.get(Servo.class, "hookServo"); // hook servo


        // Creates Phases
        final int DETECT_MINERAL  = 1;
        final int LOWER_ROBOT= 2;
        final int RETRACT_ACTUATOR = 3;
        final int DRIVE_FROM_LANDER = 4;
        final int DESTROY_MINERAL = 5;
        final int TURN_TO_EDGE = 6;
        final int DRIVE_TO_EDGE = 7;
        final int TURN_TO_DEPOT = 8;
        final int DRIVE_TO_DEPOT = 9;
        final int TURN_TO_CRATER = 10;
        final int DRIVE_TO_CRATER = 11;
        final int IDLE = 12;

        // Sets phase
        int currentPhase = LOWER_ROBOT;

        //Names all variables, sets values
        double leftWheelPower = 0; // Used for controlling power on the left drive motor.
        double rightWheelPower = 0; // Used for controlling power on the right drive motor
        double armExtendPower = 0; // Used to extend the grabbing arm
        double liftRobotPower = 0; // Used for controlling power on the lifting motor
        double armRaisePower = 0; // used to raise and lower the arm
        double linActLowerDistance = 150; // Used to change the distance we lower the robot
        double straightDriveDistance = 100; // Driving away from the lander
        double edgeDriveDistance = 100; // Driving to wall
        double depotDriveDistance = 200; // Driving to depot for team marker


        double WHEEL_POWER_MLUT = 1.5; // the power to the wheels
        double ARM_RAISE_POWER_MLUT = 0.2; // the power to the wheels
        double ARM_EXTEND_POWER_MLUT = 0.2; // the power to the wheels
        double LIFT_ROBOT_POWER_MLUT = 0.2; // Controls how fast linear actuator retracts
        double LOWER_ROBOT_POWER_MLUT = 0.2; // Controls how fast linear actuator extends

        int position = 0;
        int leftDrivePos = 0; // Used to measure the left drive motor's position
        int rightDrivePos = 0; // Used to measure the right drive motor's position
        int robotLiftPos = 0; // Used to measure the linear actuator motor's position;

        //Configures motors to use encoders and brake when no power is applied
        leftDriveMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        rightDriveMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        //armExtendingMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        //armRaisingMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        robotLiftMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);




        leftDriveMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        rightDriveMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        //armExtendingMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        //armRaisingMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        robotLiftMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        leftDriveMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        rightDriveMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        //armExtendingMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        //armRaisingMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        robotLiftMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);


        // Relays status to driver station.
        telemetry.addData("Status", "Initialized");
        telemetry.update();


        // Wait for the game to start (driver presses PLAY)
        waitForStart();
        // Start the logging of measured acceleration
        imu.startAccelerationIntegration(new Position(), new Velocity(), 1000);

        if (opModeIsActive())
        {
            /** Activate Tensor Flow Object Detection. */
            /* if (tfod != null)
            {
                tfod.activate();
            } */
            // Run until the end of the match (driver presses STOP)
            while (opModeIsActive())
            {
                // Update Variables
                leftDrivePos = leftDriveMotor.getCurrentPosition();
                rightDrivePos = rightDriveMotor.getCurrentPosition();
                robotLiftPos = robotLiftMotor.getCurrentPosition();


                switch (currentPhase)
                {
                    case DETECT_MINERAL:

                        if (tfod != null)
                        {
                            // getUpdatedRecognitions() will return null if no new information is available since
                            // the last time that call was made.
                            List<Recognition> updatedRecognitions = tfod.getUpdatedRecognitions();
                            if (updatedRecognitions != null)
                            {
                                telemetry.addData("# Object Detected", updatedRecognitions.size());
                                if (updatedRecognitions.size() == 3)
                                {
                                    int goldMineralX = -1;
                                    int silverMineral1X = -1;
                                    int silverMineral2X = -1;
                                    for (Recognition recognition : updatedRecognitions)
                                    {
                                        if (recognition.getLabel().equals(LABEL_GOLD_MINERAL))
                                        {
                                            goldMineralX = (int) recognition.getLeft();
                                        } else if (silverMineral1X == -1)
                                        {
                                            silverMineral1X = (int) recognition.getLeft();
                                        } else
                                        {
                                            silverMineral2X = (int) recognition.getLeft();
                                        }
                                    }
                                    if (goldMineralX != -1 && silverMineral1X != -1 && silverMineral2X != -1)
                                    {
                                        if (goldMineralX < silverMineral1X && goldMineralX < silverMineral2X)
                                        {
                                            telemetry.addData("Gold Mineral Position", "Left");
                                            currentPhase = LOWER_ROBOT;
                                        } else if (goldMineralX > silverMineral1X && goldMineralX > silverMineral2X)
                                        {
                                            telemetry.addData("Gold Mineral Position", "Right");
                                            currentPhase = LOWER_ROBOT;
                                        } else
                                        {
                                            telemetry.addData("Gold Mineral Position", "Center");
                                            currentPhase = LOWER_ROBOT;
                                        }
                                    }
                                    break;
                                }
                                telemetry.update();
                            }
                        }






                    case LOWER_ROBOT:

                        if (robotLiftPos < linActLowerDistance) {
                            robotLiftMotor.setPower(1 * LOWER_ROBOT_POWER_MLUT);
                        }
                        else {
                            robotLiftMotor.setPower(0);
                            hookServo.setPosition(0.5);
                            currentPhase = RETRACT_ACTUATOR;

                        }


                        break;

                    case RETRACT_ACTUATOR:

                        if (robotLiftPos > 100) {
                            robotLiftMotor.setPower(-1 * LIFT_ROBOT_POWER_MLUT);
                        }
                        else if (robotLiftPos < -100) {
                            robotLiftMotor.setPower(0);
                            currentPhase = DRIVE_FROM_LANDER;
                        }
                        break;

                    case DRIVE_FROM_LANDER:

                        if (leftDrivePos < straightDriveDistance) {
                            leftDriveMotor.setPower(1 * WHEEL_POWER_MLUT);
                            rightDriveMotor.setPower(-1 * WHEEL_POWER_MLUT);
                        }
                        else {
                            leftDriveMotor.setPower(0.0);
                            rightDriveMotor.setPower(0.0);
                            currentPhase = DESTROY_MINERAL;
                        }
                        break;


                    case DESTROY_MINERAL:

                        // Put mineral code here:

                        currentPhase = TURN_TO_EDGE;

                        break;

                    case TURN_TO_EDGE:

                        if (angles.firstAngle < 90) {
                            leftDriveMotor.setPower(1 * WHEEL_POWER_MLUT);
                            rightDriveMotor.setPower(1 * WHEEL_POWER_MLUT);
                        }
                        else {
                            leftDriveMotor.setPower(0.0);
                            rightDriveMotor.setPower(0.0);
                            currentPhase = DRIVE_TO_EDGE;
                        }

                        currentPhase = DRIVE_TO_EDGE;

                        break;

                    case DRIVE_TO_EDGE:

                        leftDriveMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                        rightDriveMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

                        if (leftDrivePos < edgeDriveDistance) {
                            leftDriveMotor.setPower(1 * WHEEL_POWER_MLUT);
                            rightDriveMotor.setPower(-1 * WHEEL_POWER_MLUT);
                        }
                        else {
                            leftDriveMotor.setPower(0.0);
                            rightDriveMotor.setPower(0.0);
                            currentPhase = TURN_TO_DEPOT;
                        }

                        break;

                    case TURN_TO_DEPOT:

                        if (angles.firstAngle < 90) {
                            leftDriveMotor.setPower(1 * WHEEL_POWER_MLUT);
                            rightDriveMotor.setPower(1 * WHEEL_POWER_MLUT);
                        }
                        else {
                            leftDriveMotor.setPower(0.0);
                            rightDriveMotor.setPower(0.0);
                            currentPhase = DRIVE_TO_DEPOT;
                        }

                        break;

                    case DRIVE_TO_DEPOT:

                        leftDriveMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                        rightDriveMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

                        if (leftDrivePos < depotDriveDistance) {
                            leftDriveMotor.setPower(1 * WHEEL_POWER_MLUT);
                            rightDriveMotor.setPower(-1 * WHEEL_POWER_MLUT);
                        }
                        else {
                            leftDriveMotor.setPower(0.0);
                            rightDriveMotor.setPower(0.0);
                            // Put team marker code here:
                            currentPhase = TURN_TO_CRATER;
                        }

                        break;

                    case TURN_TO_CRATER:

                        if (angles.firstAngle < 90) {
                            leftDriveMotor.setPower(1 * WHEEL_POWER_MLUT);
                            rightDriveMotor.setPower(1 * WHEEL_POWER_MLUT);
                        }
                        else {
                            leftDriveMotor.setPower(0.0);
                            rightDriveMotor.setPower(0.0);
                            currentPhase = DRIVE_TO_CRATER;
                        }

                        break;

                    case DRIVE_TO_CRATER:

                        leftDriveMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                        rightDriveMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

                        if (leftDrivePos < depotDriveDistance) {
                            leftDriveMotor.setPower(1 * WHEEL_POWER_MLUT);
                            rightDriveMotor.setPower(-1 * WHEEL_POWER_MLUT);
                        }
                        else {
                            leftDriveMotor.setPower(0.0);
                            rightDriveMotor.setPower(0.0);
                            currentPhase = IDLE;
                        }

                        break;

                    case IDLE:

                        // In case we want to do anything while stopped

                        break;

                }// End switching phases
            } // while op mode active
        } // if op mode active
        if (tfod != null) {
            tfod.shutdown();
        }
    } // runOpMode()

    /**
     * Initialize the Vuforia localization engine.
     */
    private void initVuforia() {
        /*
         * Configure Vuforia by creating a Parameter object, and passing it to the Vuforia engine.
         */
        VuforiaLocalizer.Parameters parameters = new VuforiaLocalizer.Parameters();

        parameters.vuforiaLicenseKey = VUFORIA_KEY;
        parameters.cameraName = hardwareMap.get(WebcamName.class, "Webcam 1");

        //  Instantiate the Vuforia engine
        vuforia = ClassFactory.getInstance().createVuforia(parameters);

        // Loading trackables is not necessary for the Tensor Flow Object Detection engine.
    }

    /**
     * Initialize the Tensor Flow Object Detection engine.
     */
    private void initTfod() {
        int tfodMonitorViewId = hardwareMap.appContext.getResources().getIdentifier(
                "tfodMonitorViewId", "id", hardwareMap.appContext.getPackageName());
        TFObjectDetector.Parameters tfodParameters = new TFObjectDetector.Parameters(tfodMonitorViewId);
        tfod = ClassFactory.getInstance().createTFObjectDetector(tfodParameters, vuforia);
        tfod.loadModelFromAsset(TFOD_MODEL_ASSET, LABEL_GOLD_MINERAL, LABEL_SILVER_MINERAL);
    }
}
