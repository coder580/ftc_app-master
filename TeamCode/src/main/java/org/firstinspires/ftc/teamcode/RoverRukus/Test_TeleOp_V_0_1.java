package org.firstinspires.ftc.teamcode.RoverRukus;
// test 10/23

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;

/**
 * This file contains an minimal example of a Linear "OpMode". An OpMode is a 'program' that runs in either
 * the autonomous or the teleop period of an FTC match. The names of OpModes appear on the menu
 * of the FTC Driver Station. When an selection is made from the menu, the corresponding OpMode
 * class is instantiated on the Robot Controller and executed.
 *
 * This particular OpMode just executes a basic Tank Drive Teleop for a PushBot
 * It includes all the skeletal structure that all linear OpModes contain.
 *
 * Remove a @Disabled the on the next line or two (if present) to add this opmode to the Driver Station OpMode list,
 * or add a @Disabled annotation to prevent this OpMode from being added to the Driver Station.
 */
@TeleOp
//@Disabled

public class Test_TeleOp_V_0_1 extends LinearOpMode {

    // Properties
    /**
     * Declares our actuators
     */
    private DcMotor leftDriveMotor; // Left driving motor
    private DcMotor rightDriveMotor; // Right drive motor
    private DcMotor armExtendMotor; //  arm extending motor
    private DcMotor armRaiseMotor; //  arm raising motor
    private DcMotor robotLiftMotor; // lifting motor
    private DcMotor sweeperMotor; // mineral intake motor
    private Servo hookServo; // lift hook servo

    @Override
    public void runOpMode() {
        // Properties
        // Gets hardware info and names each actuator
        leftDriveMotor = hardwareMap.get(DcMotor.class, "leftDriveMotor"); // Left drive motor
        rightDriveMotor = hardwareMap.get(DcMotor.class, "rightDriveMotor"); // Right drive motor
        armExtendMotor = hardwareMap.get(DcMotor.class, "armExtendMotor"); // arm extending motor
        armRaiseMotor = hardwareMap.get(DcMotor.class, "armRaiseMotor"); // arm raising motor
        robotLiftMotor = hardwareMap.get(DcMotor.class, "robotLiftMotor"); // robot lifting motor
        sweeperMotor = hardwareMap.get(DcMotor.class, "sweeperMotor"); // robot lifting motor
        hookServo = hardwareMap.get(Servo.class, "hookServo"); // hook servo

        //Names all variables, sets values
        double leftWheelPower = 0; // Used for controling power on the left drive motor.
        double rightWheelPower = 0; // Used for controling power on the right drive motor
        double armExtendPower = 0; // Used to extend the grabbbing arm
        double armRaisePower = 0; // used to raise and lower the arm


        double WHEEL_POWER_MULT = 0.2; // the power to the wheels
        double ARM_RAISE_POWER_MULT = 0.05; // the power to the wheels
        double ARM_EXTEND_POWER_MULT = 0.2; // the power to the wheels
        double LIFT_ROBOT_POWER_MULT = 1; // the power to the wheels
        double SWEEPER_POWER_MULT = 0.75; // the power of the sweeper


        double maxArmMult = 0.30; // the max power applied to the arm raising motor
        double armMultiplierSlope = maxArmMult / 4000; //slope is the change in power divided by the distance traveled

        int armRaiseMotorPos = 0; // used to measure the position of the arm

        //Configures motors to use encoders and brake when no power is applied
        leftDriveMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        rightDriveMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        armExtendMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        armRaiseMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        robotLiftMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        sweeperMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);




        leftDriveMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        rightDriveMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        armExtendMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        armRaiseMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
//  robotLiftMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
//  sweeperMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        armRaiseMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        armRaiseMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        // Relays status to driver station
        telemetry.addData("Status", "Initialized");
        telemetry.update();


        // Wait for the game to start (driver presses PLAY)
        waitForStart();

        // Run until the end of the match (driver presses STOP)
        while (opModeIsActive()) {

            armRaiseMotorPos = armRaiseMotor.getCurrentPosition();

            // Get gamepad variables
            leftWheelPower = this.gamepad1.left_stick_y * WHEEL_POWER_MULT;
            rightWheelPower = -this.gamepad1.right_stick_y * WHEEL_POWER_MULT;
            armExtendPower = this.gamepad2.right_stick_y * ARM_EXTEND_POWER_MULT;
            if (this.gamepad2.left_stick_y > 0) {
                ARM_RAISE_POWER_MULT = -armMultiplierSlope * armRaiseMotorPos + maxArmMult; // this line will make the arm slow down as it moves up, so that it won't go too fast as it falls down
                //ARM_RAISE_POWER_MULT = 0.1;
            }
            else if (this.gamepad2.left_stick_y < 0) {
                ARM_RAISE_POWER_MULT = armMultiplierSlope * armRaiseMotorPos;
                //ARM_RAISE_POWER_MULT = 0.1;
            }
            else
            {
                ARM_RAISE_POWER_MULT = 0.0;
            }
            armRaisePower = this.gamepad2.left_stick_y * ARM_RAISE_POWER_MULT;


            // Servo Commands
            if (this.gamepad2.a) {
                hookServo.setPosition(0.85);
            } else if (this.gamepad2.b) {
                hookServo.setPosition(0.35);
            } else if (this.gamepad2.x) {
                hookServo.setPosition(0.625);
            } else if (this.gamepad2.y) {
                hookServo.setPosition(0.52);
            }

            // Linear Actuator Commands

            if (this.gamepad2.dpad_up) {
                robotLiftMotor.setPower(-1 * LIFT_ROBOT_POWER_MULT);
            }
            else if (this.gamepad2.dpad_down) {
                robotLiftMotor.setPower(1 * LIFT_ROBOT_POWER_MULT);
            }
            else {
                robotLiftMotor.setPower(0);
            }
            // Sweeper Commands

            if (this.gamepad2.left_trigger > 0.15) {
                sweeperMotor.setPower(1 * SWEEPER_POWER_MULT);
            }
            else if (this.gamepad2.right_trigger > 0.15) {
                sweeperMotor.setPower(-1 * SWEEPER_POWER_MULT);
            }
            else {
                sweeperMotor.setPower(0);
            }



            // Driving commands
            leftDriveMotor.setPower(leftWheelPower);
            rightDriveMotor.setPower(rightWheelPower);
            armExtendMotor.setPower(armExtendPower);
            armRaiseMotor.setPower(armRaisePower);


            telemetry.addData("armRaiseMotorPos =", armRaiseMotorPos);
            telemetry.addData("armRaiseMult =", ARM_RAISE_POWER_MULT);
            telemetry.addData("armRaisePower = ", armRaisePower);
            telemetry.update();

            idle();
        } // while op mode active
    } // runOpMode()
}
