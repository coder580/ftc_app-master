package org.firstinspires.ftc.teamcode.RoverRukus;
// test 10/23

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.hardware.CRServo;

/**
 * This file contains an minimal example of a Linear "OpMode". An OpMode is a 'program' that runs in either
 * the autonomous or the teleop period of an FTC match. The names of OpModes appear on the menu
 * of the FTC Driver Station. When an selection is made from the menu, the corresponding OpMode
 * class is instantiated on the Robot Controller and executed.
 *
 * This particular OpMode just executes a basic Tank Drive Teleop for a PushBot
 * It includes all the skeletal structure that all linear OpModes contain.
 *
 * Remove a @Disabled the on the next line or two (if present) to add this opmode to the Driver Station OpMode list,
 * or add a @Disabled annotation to prevent this OpMode from being added to the Driver Station
 */
@TeleOp
//@Disabled

public class LinActLiftTest extends LinearOpMode {

    // Properties
    /**
     * Declares our actuators
     */
    private DcMotor motor1; // Left drive motor
//    private DcMotor motor2; // Right drive motor
//    private DcMotor motor3; //  arm extending motor
//    private DcMotor motor4; //  arm raising motor
//    private DcMotor motor5; // lifting motor
   private Servo servo1; // servo 1
      //   private CRServo CRServo1;

    @Override
    public void runOpMode() {
        // Properties
        // Gets hardware info and names each actuator
        motor1 = hardwareMap.get(DcMotor.class, "motor1"); // Left drive motor
//        motor2 = hardwareMap.get(DcMotor.class, "motor2"); // Right drive motor
//        motor3 = hardwareMap.get(DcMotor.class, "motor3"); // arm extending motor
//        motor4 = hardwareMap.get(DcMotor.class, "motor4"); // arm raising motor
//        motor5 = hardwareMap.get(DcMotor.class, "motor5"); // robot lifting motor
        servo1 = hardwareMap.get(Servo.class, "servo1"); // servo motor


        //    CRServo1 = hardwareMap.get(CRServo.class, "CRServo1");

        //Names all variables, sets values
        double leftWheelPower = 0; // Used for controling power on the left drive motor.
        double rightWheelPower = 0; // Used for controling power on the right drive motor
        double armExtendPower = 0; // Used to extend the grabbbing arm
        double liftRobotPower = 0; // Used for controlling power on the lifting motor
        double armRaisePower = 0; // used to raise and lower the arm

        double leftTrigger = 0; // Used to measure how far the left trigger is pressed
        double rightTrigger = 0; // Used to measure how far the right trigger is pressed
        double TRIGGER_MULTIPLIER = 0.2; // Used to scale down arm rotation
        double WHEEL_POWER_MLUT = 0.05; // the power to the wheels
        double ARM_RAISE_POWER_MLUT = 0.2; // the power to the wheels
        double ARM_EXTEND_POWER_MLUT = 0.2; // the power to the wheels
        double LIFT_ROBOT_POWER_MLUT = 0.2; // the power to the wheels

        int position = 0;
        int motor1pos = 0; // Used to measure the left drive motor's position
        int motor2pos = 0; // Used to measure the right drive motor's position
        int m1rotation = 0; // Used as telemetry imput variable for the "motorrotation1 =" string
        int m2rotation = 0; // Used as telemetry imput variable for the "motorrotation2 =" string
        int m1startpos = 0; // Used to help control the left drive motor during autonomous driving
        int m2startpos = 0; // used to help control the right drive motor during autonomous driving

        //Configures motors to use encoders and brake when no power is applied
        motor1.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
/*        motor2.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motor3.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motor4.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        motor5.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
*/



        motor1.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
/*        motor2.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        motor3.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        motor4.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        motor5.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
*/
        // finds current position of all motors
        m1startpos = motor1.getCurrentPosition();
//        m2startpos = motor2.getCurrentPosition();

        // Relays status to driver station
        telemetry.addData("Status", "Initialized");
        telemetry.update();


        // Wait for the game to start (driver presses PLAY)
        waitForStart();

        // Run until the end of the match (driver presses STOP)
        while (opModeIsActive()) {


            // Asigns the numbers representing how far the drive motors have rotated to m1rotation and m2rotation
            motor1pos = motor1.getCurrentPosition();
            //           motor2pos = motor2.getCurrentPosition();
            m1rotation = motor1pos - m1startpos;
            m2rotation = motor2pos - m2startpos;


            // Get gamepad variables
            leftWheelPower = this.gamepad1.left_stick_y * WHEEL_POWER_MLUT;
            rightWheelPower = -this.gamepad1.right_stick_y * WHEEL_POWER_MLUT;
            armExtendPower = this.gamepad1.right_stick_y * ARM_EXTEND_POWER_MLUT;
            armRaisePower = this.gamepad1.left_stick_y * ARM_RAISE_POWER_MLUT;
            if (this.gamepad1.a) {
                servo1.setPosition(0.85);
            } else if (this.gamepad1.b) {
                servo1.setPosition(0.35);
            } else if (this.gamepad1.x) {
                servo1.setPosition(0.625);
            } else if (this.gamepad1.y) {
                servo1.setPosition(0.52);
            }


            // Driving commands
            motor1.setPower(leftWheelPower);
            //           motor2.setPower(rightWheelPower);
            //         motor3.setPower(armExtendPower);
            //       motor4.setPower(armRaisePower);


        /*    if (this.gamepad1.a) {
                CRServo1.setDirection(DcMotor.Direction.FORWARD);
                CRServo1.setPower(1);
            } else if (this.gamepad1.b) {
                CRServo1.setDirection(DcMotor.Direction.REVERSE);
                CRServo1.setPower(1);
            }
            else {
                CRServo1.setPower(0);
            }

*/
            // Motor commands
            telemetry.addData("Motor Position =", motor1pos );
            telemetry.update();

            idle();
        } // while op mode active
    } // runOpMode()
}
